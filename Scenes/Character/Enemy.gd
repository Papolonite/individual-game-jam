extends Position2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var health_ui : TextureProgress = get_node("HealthUI")
onready var stats : Stats = get_node("Stats")
var temp


# Called when the node enters the scene tree for the first time.
func _ready():
	health_ui.max_value = stats.max_health
	health_ui.value = stats.hp
	
func get_damage(hp):
	stats.set_hp(stats.hp - hp)
	if stats.hp <= 0:
		stats.dead()
		health_ui.value = 0
	else :
		health_ui.value = stats.hp


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
