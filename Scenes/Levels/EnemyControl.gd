extends Node2D

class_name Enemy

signal sendMessageToPlayer(message )
signal enemyActionDone(type, value)
signal death
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var enemyStats = $Stats
onready var sprite = $Sprite
onready var action = $Action
onready var reaction = $Reaction
onready var menu = $HealthUI
onready var timer = Timer.new()
var temp


# Called when the node enters the scene tree for the first time.
func _ready():
	menu.max_value = enemyStats.max_health
	menu.value = enemyStats.hp

func enemyReaction(type,value):
	if type == "damage":
		reaction.get_damage(enemyStats, value)
		if value != 0 :
			sprite.play("hurt")
			yield(sprite, "animation_finished")
			sprite.play("idle")
		if (enemyStats.hp <= 0):
			enemyStats.hp = 0
			updateHealthUI()
			sprite.play("die")
			yield(sprite, "animation_finished")
			sprite.play("die_idle")
			yield(sprite, "animation_finished")
			emit_signal("death")
		updateHealthUI()
	elif type == "damageOvertime" and value == 1:
		reaction.set_damageOvertime(enemyStats)
		sprite.play("hurt")
		yield(sprite, "animation_finished")
		sprite.play("idle")
	elif type == "modifier" and value == 1:
		reaction.set_taunt(enemyStats)
		enemyStats.accuracy = enemyStats.accuracy / 4
		enemyStats.crit_chance = enemyStats.crit_chance * 2
		enemyStats.strength = enemyStats.strength * 2
		
func enemyTurn():
	if enemyStats.check_damageOvertime():
		yield(enemyReaction("damage",2), "completed")
		emit_signal("sendMessageToPlayer", "Enemy bleeds for 2 damage")
		yield(get_tree().create_timer(0.5), "timeout")
	if not enemyStats.check_taunt():
		enemyStats.accuracy = enemyStats.true_accuracy
		enemyStats.crit_chance = enemyStats.crit_chance
		enemyStats.strength = enemyStats.true_strength
	temp = action.attack(enemyStats.strength, enemyStats.accuracy, enemyStats.crit_chance)
	if temp != 0 :
		sprite.play("attack1")
		yield(sprite, "animation_finished")
		emit_signal("sendMessageToPlayer", "Enemy dealt " + String(temp) + " to the player.")
		sprite.play("idle")
	else:
		emit_signal("sendMessageToPlayer", "Enemy attack missed the player")
	emit_signal("enemyActionDone", "damage", temp)

func updateHealthUI():
	menu.value = enemyStats.hp

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

