extends Node

signal playerActionDone(type, value)
signal death

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var playerStats = get_node("Stats")
onready var menu = get_node("Menu")
onready var reaction = $Reaction
var temp : int


# Called when the node enters the scene tree for the first time.
func _ready():
	menu.disableButton()
	updatePlayerStatsUI()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func updatePlayerStatsUI():
	menu.updatePlayerStatsLabel(playerStats.hp)

func playerReaction(type,value):
	if type == "damage":
		reaction.get_damage(playerStats, value)
		if playerStats.hp <= 0 :
			playerStats.hp = 0
			emit_signal("death")
		updatePlayerStatsUI()

func _on_Menu_playerInputSignal(action):
	if action == "Attack":
		temp = get_node("Action").attack(playerStats.strength, playerStats.accuracy, playerStats.crit_chance)
		if (temp == 0):
			menu.contextMsg("Player Missed the enemy")
		elif (temp == (playerStats.strength * 2)):
			menu.contextMsg("Player Dealt " + String(temp) + " critical damage to the enemy")
		else:
			menu.contextMsg("Player Dealt " + String(temp) + " damage to the enemy")
		emit_signal("playerActionDone","damage", temp)
	elif action == "Bleed":
		temp = get_node("Action").bleed(playerStats.accuracy)
		if temp == 0 :
			menu.contextMsg("Player tried to bleed the enemy. It missed")
		else:
			menu.contextMsg("Player tried to bleed the enemy. It succeed. Bleed last 3 turn")
		emit_signal("playerActionDone","damageOvertime",temp)
	elif action == "Taunt":
		temp = get_node("Action").taunt(playerStats.accuracy)
		if temp == 0 :
			menu.contextMsg("Player Taunted the enemy. It missed")
		else:
			menu.contextMsg("Player Taunts the enemy. It succeed. Taunt last 3 turn")
		emit_signal("playerActionDone","modifier", temp)
