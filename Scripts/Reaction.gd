extends Node

class_name Reaction
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func get_damage(stats, hp):
	stats.set_hp(stats.hp - hp)

func set_damageOvertime(stats):
	stats.set_damageOvertime()

func set_taunt(stats):
	stats.set_tauntTurn()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
