extends Resource

class_name Skill

export var skill_name : String

export var damage : int
export var isDamageOvertime : bool
export var damageOvertime : int
export var hasCooldown : bool
export var cooldown : int
