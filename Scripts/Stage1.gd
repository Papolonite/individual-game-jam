extends Node2D

signal sendMessageToPlayer(message)
signal enemyActionDone(type, value)
signal death
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Enemy_enemyActionDone(type, value):
	emit_signal("enemyActionDone",type, value)


func _on_Enemy_death():
	emit_signal("death") # Replace with function body.


func _on_Enemy_sendMessageToPlayer(message):
	emit_signal("sendMessageToPlayer",message) # Replace with function body.
