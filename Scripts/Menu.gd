extends MarginContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal playerInputSignal(action)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func disableButton():
	get_node("Overlay/VBoxContainer/HBoxContainer/AttackButton").disabled = true
	get_node("Overlay/VBoxContainer/HBoxContainer/TauntButton").disabled = true
	get_node("Overlay/VBoxContainer/HBoxContainer/BleedButton").disabled = true

func enableButton():
	get_node("Overlay/VBoxContainer/HBoxContainer/AttackButton").disabled = false
	get_node("Overlay/VBoxContainer/HBoxContainer/TauntButton").disabled = false
	get_node("Overlay/VBoxContainer/HBoxContainer/BleedButton").disabled = false

func contextMsg(message : String):
	get_node("Overlay/VBoxContainer/ContextMsg").text = "> " + message

func updatePlayerStatsLabel(hp):
	get_node("Overlay/VBoxContainer/Stats").text = "Player Status: HP :" + String(hp)

func _on_AttackButton_pressed():
	emit_signal("playerInputSignal","Attack")

func _on_TauntButton_pressed():
	emit_signal("playerInputSignal","Taunt")

func _on_BleedButton_pressed():
	emit_signal("playerInputSignal","Bleed")
