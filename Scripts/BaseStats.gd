extends Resource

class_name BaseStats

export var name : String
export var max_health: int
export var strength: int
export var defense: int
export var speed: int
export(float,0.0,1.0) var accuracy : float
export(float,0.0,1.0) var crit_chance : float

var health: int
var is_alive: bool

func reset():
	health = self.max_health
	
func copy() -> BaseStats:
	# Perform a more accurate duplication, as normally Resource duplication
	# does not retain any changes, instead duplicating from what's registered
	# in the ResourceLoader
	var copy = duplicate()
	copy.health = health
	return copy
