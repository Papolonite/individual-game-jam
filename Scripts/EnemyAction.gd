extends Node

signal playerAttack(damage)
signal playerActionDone
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var tempRng : float
var rng = RandomNumberGenerator.new()
var tempStatsAttr : float
var temp

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Menu").updatePlayerStatsLabel(get_node("Stats").hp)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass





func _on_Menu_playerInputSignal(menu):
	get_node("Menu").disableButton()
	if menu == "Attack":
		temp = attack()
		emit_signal("playerAttack", temp)
		get_node("Menu").contextMsg("Player dealt " + String(temp) + " damage")
		get_node("Menu").updatePlayerStatsLabel(get_node("Stats").hp)
		emit_signal("playerActionDone")
		
func attack():
	rng.randomize()
	tempRng = rng.randf()
	tempStatsAttr = get_node("Stats").accuracy
	if tempRng < tempStatsAttr:
		tempRng = rng.randf()
		tempStatsAttr = get_node("Stats").crit_chance
		if tempRng < tempStatsAttr :
			return get_node("Stats").strength * 2
		else : 
			return get_node("Stats").strength
	else:
		return 0
	
