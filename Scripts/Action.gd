extends Node

class_name Action
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var tempRng : float
var rng = RandomNumberGenerator.new()
var temp

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func attack(strength, accuracy, crit_chance):
	rng.randomize()
	tempRng = rng.randf()
	if tempRng < accuracy:
		rng.randomize()
		tempRng = rng.randf()
		if tempRng < crit_chance :
			return strength * 2
		else : 
			return strength
	else:
		return 0

func taunt(accuracy):
	rng.randomize()
	tempRng = rng.randf()
	if tempRng < accuracy:
		return 1
	else :
		return 0
		
func bleed(accuracy):
	rng.randomize()
	tempRng = rng.randf()
	if tempRng < accuracy:
		return 1
	else :
		return 0
