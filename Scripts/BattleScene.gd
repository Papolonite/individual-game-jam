extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var level = "Stage1"
var temp : Node

# Called when the node enters the scene tree for the first time.
func _ready():
	battle()

func battle():
	playerTurn()

func enemyTurn():
	get_node("Stage1/Enemy").enemyTurn()

func playerTurn():
	get_node("Player").get_node("Menu").enableButton()

func _on_Player_playerActionDone(type, value):
	get_node("Player").get_node("Menu").disableButton()
	get_node(level).get_node("Enemy").enemyReaction(type,value)
	yield(get_tree().create_timer(1.9), "timeout")
	enemyTurn()

func _on_Stage1_enemyActionDone(type, value):
	get_node("Player").playerReaction(type,value)
	yield(get_tree().create_timer(0.7), "timeout")
	playerTurn()

 # Replace with function body.
func _on_Player_death():
	get_tree().change_scene("res://Scenes/UI/GameOver.tscn")

func _on_Stage1_death():
	get_tree().change_scene("res://Scenes/UI/WinScreen.tscn") 


func _on_Stage1_sendMessageToPlayer(message):
	get_node("Player").get_node("Menu").contextMsg(message)
