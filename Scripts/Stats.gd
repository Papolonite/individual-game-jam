extends Node

class_name Stats

export var baseStats : Resource
var hp : int
var name_char : String
var max_health: int
var strength: int
var true_strength : int
var defense: int
var speed: int
var accuracy : float
var true_accuracy : float
var crit_chance : float
var true_crit_chance : float
var isAlive = true
var damageOvertimeTurn = 0
var isTauntTurn = 0

func _initialize(stats : Resource):	
	self.name_char = stats.name
	self.max_health = stats.max_health
	self.strength = stats.strength
	self.defense = stats.defense
	self.speed = stats.speed
	self.accuracy = stats.accuracy
	self.crit_chance = stats.crit_chance
	self.true_accuracy = stats.accuracy
	self.true_crit_chance = stats.crit_chance
	self.true_strength = stats.strength
	self.hp = self.max_health

func set_hp(hp):	
	self.hp = hp
	
func set_damageOvertime():
	if self.damageOvertimeTurn != 3:
		self.damageOvertimeTurn = 3
		
func set_tauntTurn():
	if self.isTauntTurn != 3:
		self.isTauntTurn = 3
		
func check_damageOvertime():
	if self.damageOvertimeTurn != 0 :
		self.damageOvertimeTurn -= 1
		return true
	else :
		return false
		
func check_taunt():
	if self.isTauntTurn != 0 :
		self.isTauntTurn -= 1
		return true
	else :
		return false

func dead():
	self.isAlive = false

# Called when the node enters the scene tree for the first time.
func _ready():
	self._initialize(baseStats)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
